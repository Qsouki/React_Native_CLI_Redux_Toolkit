 import CheckBox from '@react-native-community/checkbox';
import React, { useState } from 'react';
 import {
   Text,
   View,
   StyleSheet
 } from 'react-native';
 
 const Content= ({data,selectedAnswer, onChange}) => {
   return (
    <View style={{flexDirection:"row",marginTop:4}}>

      <CheckBox
        boxType='square'
        onValueChange={onChange}
        value={selectedAnswer?.label === data.label }
      />
      <Text style={{marginLeft:4}}>{data.label}</Text>
    </View> 
   );
 };
 
 const styles = StyleSheet.create({
})

 export default Content;
 