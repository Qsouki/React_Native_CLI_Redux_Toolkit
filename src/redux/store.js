import { configureStore } from "@reduxjs/toolkit";
import appReducer from './appState';

export default configureStore({
  reducer: {
    appReducer
  }
});