import { createSlice } from "@reduxjs/toolkit";

export const dataSlice = createSlice({
  name: "data",
  initialState: {
    result:0,
    questionNumber:0
  },
  reducers: {
    getFetchedData: (state, action) => {
      state.result = action.payload.result
      state.questionNumber = action.payload.questionNumber
    },
  }
});

// Action creators are generated for each case reducer function
export const { getFetchedData } = dataSlice.actions;

export default dataSlice.reducer;
