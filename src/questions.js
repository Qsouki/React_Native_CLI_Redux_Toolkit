/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Content from './components/content';
import data from './data.json';
import {getFetchedData} from './redux/appState';

const Questions = () => {
  const dispatch = useDispatch();
  const [selectedAnswer, setSelectedAnswer] = useState(null);
  const [close, setClose] = useState(false);
  const [seconds, setSeconds] = useState(null);
  const [minutes, setMinutes] = useState(0);
  const [timer, setTimer] = useState(null);
  const {questionNumber, result} = useSelector(state => state.appReducer);

  useEffect(() => {
    setSeconds(data[questionNumber]?.time);
  }, [questionNumber]);

  useEffect(() => {
    if (questionNumber > 3) {
      setClose(true);
    } else {
      setTimer(
        setInterval(() => {
          if (seconds > 0) {
            setSeconds(seconds - 1);
          }
          if (seconds == 0) {
            next();
          }
        }, 1000),
      );
      return () => clearInterval(timer);
    }
  }, [seconds]);

  const next = () => {
    if(questionNumber==2){
      setClose(true)
    }
    clearInterval(timer);
    let result_ = 0;
    result_ = selectedAnswer?.correct ? 1 : 0;
    dispatch(
      getFetchedData({
        questionNumber: questionNumber + 1,
        result: result + result_,
      }),
    );
  };

  const redo = () => {
    setSelectedAnswer(null);
    dispatch(getFetchedData({questionNumber: 0, result: 0}));
    setClose(false);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <View>
          <Image
            style={styles.userIcon}
            source={require('./assets/img/user.png')}
          />
        </View>
        <Text style={styles.title}>Hi, User name</Text>
        <Text style={styles.subTitle}>aaaaa zzzzzz eeeee rrrrr</Text>
        {!close && (
          <View style={styles.subHeader}>
            <View style={styles.leftView}>
              <Text style={styles.subHeaderTxt}>Question</Text>
              <View style={styles.badge}>
                <Text style={{color: 'blue'}}>
                  {questionNumber + 1}/{data.length}
                </Text>
              </View>
            </View>
            <View style={styles.rightView}>
              <Text style={styles.subHeaderTxt}>Temps écoulé</Text>
              <View style={{flexDirection: 'row'}}>
                <View style={styles.badge}>
                  <Text style={{color: 'blue'}}>{minutes}</Text>
                </View>
                <View style={[styles.badge, {marginLeft: 2}]}>
                  <Text style={{color: 'orange'}}>{seconds}</Text>
                </View>
              </View>
            </View>
          </View>
        )}
      </View>
      {close ? (
        <View style={[styles.content, {alignSelf: 'center'}]}>
          <Text style={{textAlign: 'center'}}>Félicitation</Text>
          <Text style={{textAlign: 'center'}}>Voici votre score</Text>
          <View style={[styles.badge, {marginTop: 10, alignSelf: 'center'}]}>
            <Text style={{color: 'blue'}}>
              {result}/{data.length}
            </Text>
          </View>
          <View style={[styles.footer, {marginTop: 10}]}>
            <TouchableOpacity style={styles.btn} onPress={redo}>
              <Text style={styles.btnTxt}>Refaire le test</Text>
            </TouchableOpacity>
          </View>
        </View>
      ) : (
        <>
          <View style={styles.content}>
            <Text style={styles.contentTitle}>
              Question {questionNumber + 1}
            </Text>
            <Text>{data[questionNumber]?.label}</Text>
            {data[questionNumber]?.answers.map((item, index) => (
              <Content
                key={index}
                data={item}
                onChange={() => setSelectedAnswer(item)}
                selectedAnswer={selectedAnswer}
              />
            ))}
          </View>
          <View style={styles.footer}>
            <TouchableOpacity style={styles.btn} onPress={next}>
              <Text style={styles.btnTxt}>Suivant</Text>
            </TouchableOpacity>
          </View>
        </>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#02b3e0',
    paddingHorizontal: 25,
    paddingVertical: 30,
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    shadowColor: '#000000',
    shadowOpacity: 0.2,
    shadowRadius: 2,
    shadowOffset: {
      height: 5,
      width: 1,
    },
  },
  title: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  },
  subTitle: {
    fontSize: 15,
  },
  userIcon: {
    width: 50,
    height: 50,
    alignSelf: 'flex-end',
  },
  subHeader: {
    position: 'absolute',
    right: 30,
    left: 30,
    bottom: -40,
    backgroundColor: 'white',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 8,
    paddingHorizontal: 10,
    shadowColor: '#000000',
    shadowOpacity: 0.2,
    shadowRadius: 2,
    shadowOffset: {
      height: 5,
      width: 1,
    },
  },
  rightView: {
    flex: 1,
    alignItems: 'flex-start',
  },
  leftView: {
    flex: 1,
  },
  subHeaderTxt: {
    fontSize: 12,
  },
  badge: {
    padding: 8,
    backgroundColor: '#81d4dd',
    alignSelf: 'flex-start',
    borderRadius: 4,
  },
  footer: {
    alignSelf: 'center',
    flex:1
  },
  btn: {
    backgroundColor: '#02b3e0',
    alignSelf: 'flex-start',
    paddingVertical: 8,
    paddingHorizontal: 40,
    borderRadius: 20,
  },
  btnTxt: {
    color: 'white',
  },
  content: {
    marginTop: "30%",
    paddingHorizontal: 30,
    flex:1
  },
  contentTitle: {
    fontWeight: 'bold',
  },
});

export default Questions;
