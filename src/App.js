/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useState } from 'react';
 import { Provider } from "react-redux";
import Questions from './questions';

import store from "./redux/store";

 const App= () => {
   return (
    <Provider store={store}>
      <Questions />
    </Provider>
   );
 };

 export default App;
 